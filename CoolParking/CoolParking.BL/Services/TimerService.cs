﻿
using System.Timers;
using CoolParking.BL.Interfaces;
namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public TimerService(double interval)
        {
            Interval = interval;
        }

        public event ElapsedEventHandler Elapsed;
        private Timer _timer = new Timer();
        public double Interval { get; set; }
        public void Start()
        {
            _timer.Interval = Interval; 
            _timer.Start();
            _timer.Elapsed += Tick;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }
        public void Stop()
        {
            _timer.Stop();
        }
        public void Dispose()
        {
            Dispose();
        }

        void Tick(object s, ElapsedEventArgs e)
        {
            Elapsed(s, e);
        }
    }
}