﻿
using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;
 namespace CoolParking.BL.Services
 {
     public class LogService : ILogService
     {
         public string LogPath { get; }

         public LogService(string path)
         {
             LogPath = path;
         }
         public void Write(string logInfo)
         {
             using (StreamWriter sw = new StreamWriter(LogPath, true,Encoding.Default))
             {
                 sw.WriteLine(logInfo);
             }
         }
 
         public string Read()
         {
             if (!File.Exists(LogPath)) throw new InvalidOperationException();
             using (StreamReader sr = new StreamReader(LogPath, Encoding.Default))
             {
                 StringBuilder accumulator = new StringBuilder();
                 string line;
                 while ((line = sr.ReadLine()) != null)
                 {
                     accumulator.Append(line + "\n");
                 }
                 return accumulator.ToString();
             };
         }
     }
 }