﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService _withdrawTimer { get; }
        private ITimerService _logTimer { get; }
        private ILogService _logService { get; }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService log)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = _logService;
            _withdrawTimer.Elapsed += Pay;
            _withdrawTimer.Start();
            _logTimer.Elapsed += Log;
            _logTimer.Start();
        }
        public void Dispose()
        {
            Parking.Instance.Vehicles.Clear();
            Parking.Instance.Transactions.Clear();
            Parking.Instance.Balance = Settings.StartParkingBalance;
        }

        public decimal GetBalance()
        {
            return Parking.Instance.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Instance.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Instance.Capacity - Parking.Instance.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var readOnlyCollection = new ReadOnlyCollection<Vehicle>(Parking.Instance.Vehicles);
            return readOnlyCollection;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0) throw new InvalidOperationException();
            if ((Parking.Instance.Vehicles.Any(v => v.Id == vehicle.Id))) throw new ArgumentException();
            Parking.Instance.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (Parking.Instance.Vehicles.Any(v => v.Id == vehicleId))
            {
                var vehicle = Parking.Instance.Vehicles.First(v => v.Id == vehicleId);
                if (vehicle.Balance < 0) throw new InvalidOperationException();
                Parking.Instance.Vehicles.Remove(vehicle);
            }
            else throw new ArgumentException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (Parking.Instance.Vehicles.Any(v => v.Id == vehicleId) && (sum > 0))
            {
                var vehicle = Parking.Instance.Vehicles.First(v => v.Id == vehicleId);
                vehicle.Balance += sum;
            }
            else throw new ArgumentException();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Instance.Transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }
        private void Pay(object source, ElapsedEventArgs e)
        {
            foreach (var vehicle in Parking.Instance.Vehicles)
            {
                var price = Settings.PriceList[vehicle.VehicleType];
                if (vehicle.Balance < price)
                {
                    if (vehicle.Balance > 0) price = (price - vehicle.Balance) * Settings.Fine + vehicle.Balance;
                    else
                    {
                        price *= Settings.Fine;
                    }
                }

                vehicle.Balance -= price;
                Parking.Instance.Balance += price;
                Parking.Instance.Transactions.Add(new TransactionInfo(Vehicle.GenerateRandomRegistrationPlateNumber(),
                    DateTime.Now, price));

            }
        }
        private void Log(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("New Transaction");
            StringBuilder accumulator = new StringBuilder();
            foreach (var transaction in Parking.Instance.Transactions)
            {
                accumulator.Append("Identifier: " + transaction.VehicleId.ToString() + " DateTime: " + 
                                   transaction.TransactionDate.ToString() +  " Transaction Amount: " + 
                                   transaction.Sum.ToString() + "\n");
                
            }
            _logService.Write(accumulator.ToString());
            Parking.Instance.Transactions.Clear();
        }
    }
    
}