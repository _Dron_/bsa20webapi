﻿
using System;

namespace CoolParking.BL.Models
{
  public class TransactionInfo
   {
       public TransactionInfo(string vehicleId, DateTime transactionDate, decimal sum)
       {
           VehicleId = vehicleId;
           TransactionDate = transactionDate;
           Sum = sum;
           
       }
       public string VehicleId { get; }
       public DateTime TransactionDate { get; }
       public decimal Sum { get; set; }
       public override string ToString()
       {
           return "ID= " + VehicleId + "\nTime= " + TransactionDate + "\nSum= " + Sum;
       }
   }
}