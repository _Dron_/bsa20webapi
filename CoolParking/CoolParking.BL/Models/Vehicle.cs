﻿
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
   public class Vehicle
   {
      public decimal Balance { get; set; }
      public VehicleType VehicleType { get; }
      public string Id { get; }
        
      public Vehicle(string id, VehicleType vehicleType, decimal balance)
      {
         if ((balance < 0)) throw new ArgumentException();
         Id = id;
         VehicleType = vehicleType;
         Balance = balance;
      }
      
    
      
      public static string GenerateRandomRegistrationPlateNumber()
      {
         string[] cities = 
         {
            "AA", "AB", "BH", "CA", "BK"
         };
         string[] random =
         {
            "СН", "PO", "TE"
         };
         Random rand = new Random();
         return (cities[rand.Next(0, 4)] +"-" +rand.Next(0, 9999).ToString() + "-"+random[rand.Next(0, 2)]);
      }
   
   }
}