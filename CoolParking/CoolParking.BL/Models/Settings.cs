﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
   public static class Settings
   {
      public static decimal StartParkingBalance { get; } = 0;
      public static int PaymentInterval { get; } = 5000;
      public static int LoggingInterval { get; } = 60000;
      public static int MaxSlots { get;  } = 10;
      public static decimal Fine { get; }= 2.5M;
      public static string LoggingFileName = "Transactions.log";

      public static ReadOnlyDictionary<VehicleType, decimal> PriceList = new ReadOnlyDictionary<VehicleType, decimal>
      (new Dictionary<VehicleType, decimal>(4)
      {
         { VehicleType.Bus, 3.5M },
         { VehicleType.Motorcycle, 1 },
         { VehicleType.Truck, 5 },
         { VehicleType.PassengerCar, 2 }
      });
   }
}