﻿
using System;
using System.Collections.Generic;
namespace CoolParking.BL.Models
 {
     public class Parking
     {
         private static readonly Lazy<Parking> LazyParking = new Lazy<Parking>(() => new Parking());
         public static Parking Instance => LazyParking.Value;
         public List<Vehicle> Vehicles { get; }
         public List<TransactionInfo> Transactions { get; }
         public decimal Balance { get; set; }
         public int Capacity { get;}

         private Parking()
         {
             Vehicles = new List<Vehicle>();
             Transactions = new List<TransactionInfo>();
             Balance = Settings.StartParkingBalance;
             Capacity = Settings.MaxSlots;
         }
     }
 }
