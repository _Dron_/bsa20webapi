﻿using System.IO;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Services
{
    public class ServicesModel
    {
        public static TimerService _withdrawTimer = new TimerService(Settings.PaymentInterval);
        public static TimerService _logTimer = new TimerService(Settings.LoggingInterval);
        public static LogService _logService = new LogService(Directory.GetCurrentDirectory());
        public static ParkingService _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);
    }
}