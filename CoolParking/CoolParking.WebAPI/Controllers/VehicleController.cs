﻿﻿using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers
{
    
        [Produces("application/json")]
        [Route("api/[controller]")]
        public class VehicleController : Controller
        {
            // GET : api/Vehicle
            [HttpGet]
            public JsonResult GetVehicles()
            {
                return Json(ServicesModel._parkingService.GetVehicles());
            }

           // GET : api/vehicle/id
            [HttpGet("{number}")]
            public JsonResult GetVehicle(string number)
            {
                Vehicle vehicle = Parking.Instance.Vehicles.Find((v)=> { return v.Id == number; });
                if (vehicle == null)
                    HttpContext.Response.StatusCode = 404;
                return Json(vehicle);
            }

            // POST : api/Vehicle
            [HttpPost]
            public JsonResult AddVehicle([FromBody]Vehicle vehicle)
            {
                try
                {
                    ServicesModel._parkingService.AddVehicle(vehicle);
                    HttpContext.Response.StatusCode = 201; 
                }
                catch (System.Exception ex)
                {
                    HttpContext.Response.StatusCode = 406; 
                    return Json(ex.Message);
                }
                return Json(vehicle);
            }

            // DELETE : api/vehicle/number
            [HttpDelete("{number}")]
            public void DeleteVehicle(string number)
            {
                
                if(number==null)
                {
                    HttpContext.Response.StatusCode = 404;
                    return;
                }
                try
                {
                    ServicesModel._parkingService.RemoveVehicle(number);
                }
                catch (System.Exception)
                {
                    HttpContext.Response.StatusCode = 402;  
                    return;
                }
                HttpContext.Response.StatusCode = 204;
            }
        }
    
}