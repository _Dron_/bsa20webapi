﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
   
        [Produces("application/json")]
        [Route("api/[controller]/[action]")]
        public class TransactionsController : Controller
        {

            // GET : api/transactions/TransactionLog/
            [HttpGet]
            public JsonResult TransactionLog()
            {
                string log = ServicesModel._parkingService.ReadFromLog();
                return Json(log);
            }

            // GET : api/transactions/Transactions/
            [HttpGet]
            public JsonResult Transactions()
            {
                TransactionInfo[] transactions = ServicesModel._parkingService.GetLastParkingTransactions();
                return Json(transactions.ToString());
            }


            // PUT: api/transactions/AddCarBalance/{number}
            [HttpPut("{number}")]
            public JsonResult TopUpVehicle(string number, decimal sum)
            {
                Vehicle vehicle = Parking.Instance.Vehicles.Find((v) => { return v.Id == number; });
                if (vehicle == null)
                {
                    HttpContext.Response.StatusCode = 404;
                    return null;
                }

                vehicle.Balance += sum;
                var newBalance = new {newBalance = vehicle.Balance};
                return Json(newBalance);
            }
        }
    
}