﻿﻿using System.IO;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Services;


namespace CoolParking.WebAPI.Controllers
{
    
        
        [Produces("application/json")]
        [Route("api/[controller]/[action]")]
        public class ParkingController : Controller
        {
            
            // GET : api/parking/FreePlaces
            [HttpGet]
            public JsonResult FreePlaces()
            {
                var free = new { freePlaces = ServicesModel._parkingService.GetFreePlaces()};
                return Json(free);
            }

            // GET : api/parking/Capacity
            [HttpGet]
            public JsonResult Capacity()
            {
                var capacity = new { occupiedPlaces = ServicesModel._parkingService.GetCapacity()};
                return Json(capacity);
            }

            // GET : api/parking/balance
            [HttpGet]
            public JsonResult Balance()
            {
                var balance = new { balance = ServicesModel._parkingService.GetBalance()};
                return Json(balance);
            }
        }
    
}